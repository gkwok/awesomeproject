import React from 'react';
import axios from 'axios';
import queryString from 'query-string';
import {
  Button,
  View,
  WebView,
} from 'react-native';
import { WebBrowser } from 'expo';

const s2sParameters = {
  "device": {
    "locale": "en-us",
    "lang": "en",
    "os": "9.2",
    "id": "06043b1c-4f16-385a-b026-a4b6677c684b",
    "manufacturer": "Apple"
  },
  "sdk_version": "1.0",
  "user": {
    "age": 24
  },
  "test": true,
  "app": {
    "app_key": "3f9fd04fe55f51b4166fd1e13a92d6b8"
  },
  "web_user_id": "33777e868d75f0af52bd06d8678cd74f96ee17250da7a6f813f8ff703ef4016f",
  "moment": {
    "id": "test"
  },
  "location": {
    "lat": 0,
    "lng": 0
  }
}

export default class KiipScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      kiipRewardUrl: null,
    };
  }

  componentDidMount() {
    this.loadReward();
  }

  onShouldStartLoadWithRequest(data) {
    console.log(data.url);
    if (data.url.indexOf('kiip://') >= 0) {
      const kiipUrl = data.url.replace('kiip://', '');
      const [
        command,
        parameterString,
      ] = kiipUrl.split('?');
      parameters = parameterString && queryString.parse(parameterString);
      switch(command) {
        // This is the command send for closing the reward unit.
        case 'did_dismiss':
          if (parameters) {
            console.log(parameters);
            // If there is a native_url, it means a redirection on engagement.
            if (parameters.native_url) {
              WebBrowser.openBrowserAsync(parameters.native_url);
            }
          }
          this.setState({
            kiipRewardUrl: null,
          })
          break;
        // This is for virtual currency on impression.
        case 'content':
          // See docs.kiip.me for more info on how to deal with this.
          if (parameters) {
            console.log(parameters);
          }
          break;
      }
      return false;
    }
    return true;
  }

  loadReward() {
    const r = Date.now();
    axios.post(`http://api.kiip.me/2.0/server/moment/?r=${r}`, s2sParameters)
    .then((response) => {
      this.setState({
        kiipRewardUrl: response.data.view.modal.body_url
      });
    })
    .catch((error) => {
      console.log(error);
    });
  }

  render() {
    const {
      kiipRewardUrl,
    } = this.state;

    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <Button
          title="Load Reward"
          onPress={() => this.loadReward()}
        />
        { kiipRewardUrl &&
        <WebView
          source={{uri: kiipRewardUrl}}
          onShouldStartLoadWithRequest={(data) => this.onShouldStartLoadWithRequest(data)}
        />
        }
      </View>
    );
  }
}
